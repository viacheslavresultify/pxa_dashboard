<?php

if (!defined ('TYPO3_MODE'))
	die('Access denied.');

if (!is_array($TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['pxa_dashboard_google'])) {
	$TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['pxa_dashboard_google'] = array(
		'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\StringFrontend',
		'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\FileBackend',
	);
}

?>