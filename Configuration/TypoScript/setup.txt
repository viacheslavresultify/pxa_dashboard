module.tx_pxadashboard.settings {
	controlPanel {

		googleAnalytics {

			VisitsByBrowserHome {
				label = Visits By Browser Last 2 Weeks
				chartType = PieChart
				strtotime {
					from = -13 days
					to = now
				}
				metrics = ga:visits
				optParams {
					dimensions = ga:browser
					sort = -ga:visits
				}
				headers {
					0 = Browser
					1 = Visits
				}
				options {
				}
				converts {
					1 = int
				}
			}

			VisitsByCountryHome {
				label = Visits By Country Last 2 weeks
				chartType = GeoChart
				strtotime {
					from = -13 days
					to = now
				}
				metrics = ga:visits
				optParams {
					dimensions = ga:country
					sort = -ga:visits
				}
				headers {
					0 = Country
					1 = Visits
				}
				options {
					colors {
						0 = #ffd200
						1 = #ff4800
					}
					chartArea {
						height = 400
					}
				}
				converts {
					1 = int
				}
			}

			VisitsByTypeHome {
				label = Visits By Type Last 2 weeks
				chartType = LineChart
				strtotime {
					from = -13 days
					to = now
				}
				metrics = ga:visitors
				optParams {
					dimensions = ga:year, ga:month, ga:day, ga:visitorType
					sort = ga:day
				}
				headers {
					0 = Date
					1 = New visitors
					2 = Returning visitors
				}
				options {
					titlePosition = none
					legend {
						position = bottom
					}
					pointSize = 2
					series {
						0 {
							color = green
						}
						1 {
							color = orange
						}
					}
					chartArea {
						width = 90%
						top = 10%
						bottom = 20%
					}
				}
				reArrangeDataByDate {
						# yearIndex, the key of the year in array
	 				yearIndex = 0
	 					# monthIndex, the key of the month in array
					monthIndex = 1
	 					# dayIndex, the key of the day in array
	 				dayIndex = 2
	 					# indexValuesToBuild, array of indexes for 'key' => 'values' to build for each date
					indexValuesToBuild {
						3 = 4
					}
					mappings {
						dateFormat = d/m
						columns {
							1 {
								key = New Visitor
							}
							2 {
								key = Returning Visitor
							}
						}
					}
				}
				converts {
					4 = int
				}
			}

			VisitsByTypeStatistics {
				label = Visits By Type Last 3 months
				chartType = LineChart
				strtotime {
					from = -3 months
					to = now
				}
				metrics = ga:visitors
				optParams {
					dimensions = ga:year, ga:month, ga:day, ga:visitorType
					sort = ga:day
				}
				headers {
					0 = Date
					1 = New visitors
					2 = Returning visitors
				}
				options {
					titlePosition = none
					legend {
						position = bottom
					}
					pointSize = 3
					series {
						0 {
							color = green
						}
						1 {
							color = orange
						}
					}
					height = 400
					chartArea {
						width = 90%
						top = 10%
						bottom = 20%
					}
				}
				reArrangeDataByDate {
						# yearIndex, the key of the year in array
	 				yearIndex = 0
	 					# monthIndex, the key of the month in array
					monthIndex = 1
	 					# dayIndex, the key of the day in array
	 				dayIndex = 2
	 					# indexValuesToBuild, array of indexes for 'key' => 'values' to build for each date
					indexValuesToBuild {
						3 = 4
					}
					mappings {
						dateFormat = Y-m-d
						columns {
							1 {
								key = New Visitor
							}
							2 {
								key = Returning Visitor
							}
						}
					}
				}
				converts {
					4 = int
				}
			}

			VisitsByCountryStatistics {
				label = Visits By Country Last 3 months
				chartType = GeoChart
				strtotime {
					from = -3 months
					to = now
				}
				metrics = ga:visits
				optParams {
					dimensions = ga:country
					sort = -ga:visits
				}
				headers {
					0 = Country
					1 = Visits
				}
				options {
					colors {
						0 = #ffd200
						1 = #ff4800
					}
					chartArea {
						height = 400
					}
				}
				converts {
					1 = int
				}
			}

		}

		actions {
			homeAction {
				columns {
					left {
						span = 2
						content {
							actions-list {
								partial = ControlPanel/ActionsList
								params {
									items = actions
								}
							}
							recent-updated {
								partial = ControlPanel/RecentChangesList
								params {
									items = recentlyChangedUser
								}
							}
						}
					}
					middle {
						span = 6
						content {
							feedPixelant {
								partial = ControlPanel/Feed
								params {
									feedName = pixelant
								}
							}
							feedCompany {
								partial = ControlPanel/Feed
								params {
									feedName = typo3_org
								}
							}
						}
					}
					right {
						span = 4
						content {
							gsVisitorByType {
								partial = ControlPanel/GoogleAnalyticsChart
								params {
									title = Visits by type last 2 weeks
									GoogleAnalyticsTSConfig = VisitsByTypeHome
								}
							}
							gsVisitsByCountryHome {
								partial = ControlPanel/GoogleAnalyticsChart
								params {
									title = Visits by country last 2 weeks
									GoogleAnalyticsTSConfig = VisitsByCountryHome
								}
							}
							gsVisitsByBrowserHome {
								partial = ControlPanel/GoogleAnalyticsChart
								params {
									title = Visits by browser last 2 weeks
									GoogleAnalyticsTSConfig = VisitsByBrowserHome
								}
							}
						}
					}
				}
			}
			statisticsAction {
				columns {
					full {
						span = 12
						content {
							gsVisitsByTypeStatistics {
								partial = ControlPanel/GoogleAnalyticsChart
								params {
									title = Visits by type last 3 months
									GoogleAnalyticsTSConfig = VisitsByTypeStatistics
								}
							}
							gsVisitsByCountryStatistics {
								partial = ControlPanel/GoogleAnalyticsChart
								params {
									title = Visits by country last 3 months
									GoogleAnalyticsTSConfig = VisitsByCountryStatistics
								}
							}
						}
					}
				}
			}
		}
		tabs {
			home {
				title = Home
				icon = icon-home
				actionName = homeAction
				linkAction = home
			}
			statistics {
				title = Statistics
				icon = icon-bar-chart
				actionName = statisticsAction
				linkAction = statistics
			}
			/*
			configuration {
				title = Configuration
				icon = icon-cog
				actionName = configurationAction
				linkAction = configuration
			}
			*/
		}

		rssFeeds {
			pixelant {
				url = http://www.pixelant.se/index.php?id=1&type=100
				limit = 4
				title = Pixelant.se: Nyheter
			}
			company {
				url = https://rss.mention.net/3scm/6v59/a/6yDoTCVSr_PRbW0MVXh_iFNLrsQ=
				title = Company RSS
			}
			typo3_org {
				url = http://typo3.org/xml-feeds/rss.xml
				title = TYPO3.org: Latest News
				limit = 3
			}

		}
	}

	googleApi {
		serviceAccount {
			base64Key = {$tx_pxadashboard.config.control.googleApiBase64Key}
			privateKey = {$tx_pxadashboard.config.control.googleApiPrivateKey}
			keyPassword = {$tx_pxadashboard.config.control.googleApiKeyPassword}
			email = 822917785051-n32bfcudvrtgcaborhs7g0s6p7ja1bi9@developer.gserviceaccount.com
			clientId = 822917785051-n32bfcudvrtgcaborhs7g0s6p7ja1bi9.apps.googleusercontent.com
		}

		analytics {
			accountId = 13294871
			webPropertyId = UA-13294871-6
		}
	}
}